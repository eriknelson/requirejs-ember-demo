(function(root){
  var deps = [
    "jquery",
    "ember",
    "routes/IndexRoute",
    "controllers/IndexController",
    "routes/OneRoute",
    "controllers/OneController",
    "strayboots/managers",
    "strayboots/util"
  ]
  require(deps, function(
    $, Ember, IndexRoute, IndexController, OneRoute, OneController, Managers, Util){

    root["App"] = App = Ember.Application.create();

    App.Router.map(function(){
      this.route("one", {path: "/one"});
    });

    App.set("IndexRoute", IndexRoute);
    App.set("IndexController", IndexController);
    App.set("OneRoute", OneRoute);
    App.set("OneController", OneController);


    var eMan = Managers.EventManager.create("first");
    var gMan = Managers.GuiManager.create("thing");

    eMan.sayHello();
    gMan.sayHello();

    var h = Util.Hash.hash("test");
    var u = Util.Transform.execute("foobar");
    console.log(h);
    console.log(u);

  });
})(this);
