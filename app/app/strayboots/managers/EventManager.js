define({
  create: function(name){
    var retObj = {
      _name: name,
      sayHello: function(){
        console.log("===");
        console.log("EventManager");
        console.log("===")
        console.log(this._name);
        console.log("===");
      }
    }

    return retObj
  }
});
