var deps = [
  "managers/EventManager",
  "managers/GuiManager"
]
define(deps, function(EventManager, GuiManager){
  var Managers = {
    EventManager: EventManager,
    GuiManager: GuiManager
  };
  return Managers;
});
