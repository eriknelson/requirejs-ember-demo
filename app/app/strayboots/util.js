var deps = [
  "util/Hash",
  "util/Transform"
]
define(deps, function(Hash, Transform){
  var util = {
    Hash: Hash,
    Transform: Transform
  };
  return util;
});
