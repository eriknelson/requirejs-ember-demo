define(["jquery", "ember"], function($, Ember){
  var IndexRoute = Ember.Route.extend({
    init: function(){
      console.log("IndexRoute::init");
    },
    setupController: function(controller, model){
      var self = this;
      Ember.run.scheduleOnce('afterRender', this, function(){
        $("#gotoOne").on("click", function(e){
          self.transitionTo("one");
        });
      });
    },
  });
  return IndexRoute;
});
