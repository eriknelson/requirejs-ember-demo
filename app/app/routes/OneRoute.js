define(["jquery", "ember"], function($, Ember){
  var OneRoute = Ember.Route.extend({
    init: function(){
      console.log("OneRoute::init");
    },
    setupController: function(controller, model){
      var self = this;
      controller.set("model", model);
      Ember.run.scheduleOnce('afterRender', this, function(){
        $("#gotoIndex").on("click", function(e){
          self.transitionTo("index");
        });
      });
    }
  });
  return OneRoute;
});
