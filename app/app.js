requirejs.config({
  shim: {
    "jquery": {
      exports: "$"
    },
    "ember": {
      deps: ["jquery", "handlebars"],
      exports: "Ember"
    }
  },
  paths: {
    "jquery": "lib/jquery-1.10.2.min",
    "handlebars": "lib/handlebars-1.0.0",
    "ember": "lib/ember",
    "controllers": "app/controllers",
    "routes": "app/routes",
    "strayboots": "app/strayboots",
    "managers": "app/strayboots/managers",
    "util": "app/strayboots/util"
  }
});

require(["app/main"]);
